package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {
	
	@Test
	public void f1Test() throws Exception {
		MinMax c = new MinMax ();
		assertEquals("6 is more than 5",6,c.f(5,6));
	}

	@Test
	public void f2Test() throws Exception {
		MinMax c = new MinMax ();
		assertEquals("7 is more than 3",7,c.f(7,3));
	}


	@Test
	public void bar1Test() throws Exception {
		MinMax c = new MinMax ();
		assertEquals("Not empty","I'm not empty.",c.bar("I'm not empty."));
	}

	@Test
	public void bar2Test() throws Exception {
		MinMax c = new MinMax ();
		assertEquals("Empty","",c.bar(""));
	}
	/*
	@Test
	public void bar3Test() throws Exception {
		MinMax c = new MinMax ();
		assertEquals(c.bar(null));
	}*/
	

}
